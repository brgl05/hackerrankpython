def count_substring(string, sub_string):
    #return string.count(sub_string)
    #return len(string.split(sub_string))
    count = 0
    sup = len(sub_string)
    for i in range(len(string) - sup+1):
         count += (string[i:(i+sup)]).count(sub_string)
         # .startswith is better for runtime
            

    return count  


if __name__ == '__main__':
    string = input().strip()
    sub_string = input().strip()
    
    count = count_substring(string, sub_string)
    print(count)