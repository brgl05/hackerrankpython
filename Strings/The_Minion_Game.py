import string, re
vowels  = ["A","E","I","O","U"]
consonants = [x for x in [y for y in string.ascii_uppercase] if x not in vowels]

# return in (letter,index)
def find_all_vowels(string):
    result = []
    for i in range(len(string)):
        if string[i] in vowels:
            result.append((string[i],i))
    return result     
       
    # return list of (letter,index)
def find_all_consonants(string):
    result = []
    for i in range(len(string)):
        if string[i] in consonants:
            result.append((string[i],i))
    return result   

# res = [ a, an , ann ,]     
def give_letter_combi(string):
    result = []
    for i in range(len(string)):
        result.append(string[:i+1])
    return result         


def count_letter_combi(string):
        return len(string)     
    
    
    # True == stuard False == Kevin
def start(string, name):
    result = 0
    start_letters = []
    if (name == 'stuard'):
        start_letters = find_all_consonants(string)
    elif(name == 'kevin'):
        start_letters = find_all_vowels(string)
    else:
        return ""        
    
    for x in start_letters:
        #result = result + len(give_letter_combi(string[x[1]:]))
        result = result + len(string[x[1]:])
    return result
    

    
def fast_find_all_vowels(string):
    result = 0
    for i in range(len(string)):
        if string[i] in vowels:
            result = result + len(string) - i 
    return result     
       
    # return list of (letter,index)
def fast_find_all_consonants(string):
    result = 0
    for i in range(len(string)):
        if string[i] in consonants:
             result = result + len(string) - i
    return result    
    
    
    
    
def fast_start(string, name):
    result = 0
    start_letters = []
    if (name == 'stuard'):
        return fast_find_all_consonants(string)
    elif(name == 'kevin'):
        return fast_find_all_vowels(string)
    else:
        return ""     
    
    
    
def minion_game(string):
    stuard = fast_start(string,"stuard")
    kevin = fast_start(string,"kevin")
      
    #score_stuard = sum([string.count(x) for x in stuard])
    #score_kevin = sum([string.count(x) for x in kevin])
    #if score_stuard > score_kevin:
    #elif score_kevin > score_stuard:
    if stuard > kevin:
        print ("Stuart %s" % stuard)
    elif kevin > stuard:
        print ("Kevin %s" % kevin)
    else:
        print("Draw")
            
   

if __name__ == '__main__':
    s = input()
    minion_game(s)
