import antigravity 
def swap_case(s):
    return "".join(map(lambda x: x.upper() if (x in "abcdefghijklmnopqrestuvwxyz") else x.lower(),s))
    
# einfacher im nachhinein wäre if x.lower()    
   
if __name__ == '__main__':
    s = input()
    result = swap_case(s)
    print(result)