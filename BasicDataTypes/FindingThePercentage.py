from pip._vendor.distlib.compat import raw_input

if __name__ == '__main__':
    n = int(raw_input())
    student_marks = {}
    for _ in range(n):
        line = raw_input().split()
        name, scores = line[0], line[1:]
        scores = map(float, scores)
        student_marks[name] = scores
    query_name = raw_input()

    floatnr = str((sum(student_marks[query_name]) / len(student_marks[query_name])))

    l = floatnr.split(".")
    finalstr = str(l[0]) + "."
    if (len(l[1]) == 1):
        finalstr += str(l[1]) + "0"
    elif (len(l[1]) > 2):
        finalstr += str(l[1][:2])
    else:
        finalstr += str(l[1])

    print(finalstr)