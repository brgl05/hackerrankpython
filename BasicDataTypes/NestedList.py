lst = [[raw_input(),float(raw_input())] for _ in xrange(int(raw_input()))]
lst.sort(key=lambda x: x[1])


smallest = lst[0][1]
res = [['s',0.0]]


for i in range(len(lst)):
    if (len(res) == 1):
        if (lst[i][1]) > smallest:
            res.append(lst[i])
    elif (lst[i][1]) == (res[1][1]):
        res.append(lst[i])
    else:
        break

res.remove(['s',0.0])

res.sort(key=lambda x: x[0])

for i in range(len(res)):
    print(res[i][0])
