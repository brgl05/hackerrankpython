def fun(s):
    # return True if s is a valid email, else return False
    a = s.split(".")
    if ((len(a) != 2)) : return False
    extension = a[1]
    b = a[0].split("@")
    if ((len(b) != 2)) : return False
    name = b[0]
    website = b[1]

    if (len(name) == 0 ) : return False

    if ((len(extension))>3) : return False
    
 
    if ((len(list(filter(lambda x: x.isalpha() or x.isdigit(), website)))) < len(website) ): return False
    
    if ((len(list(filter(lambda x: x.isalpha() or x.isdigit() or x == "_" or x == "-", name)))) < len(name) ): return False

    return True

    



def filter_mail(emails):
    return filter(fun, emails)

if __name__ == '__main__':
    n = int(raw_input())
    emails = []
    for _ in range(n):
        emails.append(raw_input())

    filtered_emails = filter_mail(emails)
    filtered_emails.sort()
    print filtered_emails