#!/bin/python

import math
import os
import random
import re
import sys
import datetime

# Complete the time_delta function below.
def time_delta(t1, t2):
    print("----------------------")
    print(t1)
    print(t2)
    
    all_of_t1 = t1.split(" ")
    all_of_t2 = t2.split(" ")
    
    t1_clock = all_of_t1[4].split(":")
    t2_clock = all_of_t2[4].split(":")
 
  
    #Uhrzeit unterschied
    sek_diff = int(t1_clock[2]) - int(t2_clock[2])
    min_diff = (int(t1_clock[1]) - int(t2_clock[1])) * 60
    hour_diff = (int(t1_clock[0]) - int(t2_clock[0])) * 3600
    
    clock_diff_ges = sek_diff + min_diff + hour_diff 
    
    #print("sek_diff = " + str(sek_diff) + "\t min_diff" + str(min_diff) + "\t hour_diff = " + str(hour_diff))
    
    dic = {
    "Jan": 1,"Feb": 2,
  "Mar": 3,
  "Apr": 4,
  "May" :5,
  "Jun" :6,
  "Jul" : 7,
  "Aug" :8,
  "Sep" :9,
  "Oct" :10,
  "Nov" :11,
  "Dec" :12,
}
    print(dic[(all_of_t1[2])])
    
    x = datetime.datetime( int(all_of_t1[3]),(dic[(all_of_t1[2])]),int(all_of_t1[1]))
    y = datetime.datetime(int(all_of_t2[3]),(dic[(all_of_t2[2])]),int(all_of_t2[1]))
    day_diff_obj = x - y # -397 days, 0:00:00
    
    print(day_diff_obj)
    day_diff = day_diff_obj.days
    print(day_diff_obj.days)
    
    t1_zone_diff = all_of_t1[5]
    t2_zone_diff = all_of_t2[5]
    
    # + or -
    t1_zone_op = t1_zone_diff[0]   
    t2_zone_op = t2_zone_diff[0]
    
    t1_zone_hours = (int(t1_zone_diff[1]) * 10 + int(t1_zone_diff[2])) * 3600
    t2_zone_hours = (int(t2_zone_diff[1]) * 10 + int(t2_zone_diff[2])) * 3600
    
    t1_zone_min = (int(t1_zone_diff[3]) * 10 + int(t1_zone_diff[4])) * 60
    t2_zone_min = (int(t2_zone_diff[3]) * 10 + int(t2_zone_diff[4])) * 60
    
    t1_zone_ges = (t1_zone_hours + t1_zone_min) 
    t2_zone_ges = (t2_zone_hours + t2_zone_min) 
    
    if (t1_zone_op == "+"):
        t1_zone_ges = t1_zone_ges * -1
    
    if t2_zone_op == "+":
        t2_zone_ges = t2_zone_ges * -1
    
        
    day_diff_ges = day_diff * 24 * 3600    
    utc_ges =  t1_zone_ges - t2_zone_ges
    print("day_diff = " + str(day_diff))
    print("utc_ges = " + str(utc_ges))
    print("gesamtTime " + str(clock_diff_ges))
    return str(abs(day_diff_ges + utc_ges + clock_diff_ges))
    
    
    
if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    t = int(raw_input())

    for t_itr in xrange(t):
        t1 = raw_input()

        t2 = raw_input()

        delta = time_delta(t1, t2)

        fptr.write(delta + '\n')

    fptr.close()
